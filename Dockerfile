FROM  node:16.17.1-alpine3.15

WORKDIR /usr/src/demo-argocd-app-react

# add `/app/node_modules/.bin` to $PATH
ENV PATH /usr/src/demo-argocd-app-react/node_modules/.bin:$PATH

ARG NODE_ENV
ENV NODE_ENV $NODE_ENV

COPY package*.json ./
RUN npm install

COPY . .

ENV PORT 3000
EXPOSE $PORT
CMD [ "npm", "start" ]